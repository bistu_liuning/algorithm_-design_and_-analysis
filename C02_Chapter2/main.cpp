/**
 * @file main.cpp
 * @brief 主程序
 * @author LiuNing (ning.liu@bistu.edu.cn)
 * @version 1.0
 * @date 2022-09-05
 * 
 * @copyright Copyright (c) 2022  高动态导航技术北京市重点实验室
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>日期       <th>版本     <th>作者     <th>描述     <th>时间
 * <tr><td>2022-09-05 <td>1.0     <td>刘宁     <td>第二章运行示例   <td>2108111207
 * </table>
 */
#include <QCoreApplication>
#include <gloabaluserdefine.h>
#include <recursionfunc.h>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    recursionFunc *mRecurFunc;
    mRecurFunc = new recursionFunc;

    cout<<"Algorithm Design & Analysis"<<endl;
    cout<<"Chapter 2,section 1"<<endl;
    cout<<"*********************************************"<<endl;
    uint8_t mode=RecursionType::TypeFactorial;
    switch (mode) {
    case TypeFactorial:{
        cout<<"factorial Test->Input Num:";
        uint32_t InputData;
        cin>>InputData;
        cout<<"factorial Test->Result:"<<mRecurFunc->factorial(InputData)<<endl;
    }break;
    case TypeFibonacci:{
        cout<<"fibonacci Test->Input Num:";
        uint32_t InputData;
        cin>>InputData;
        cout<<"fibonacci Test->Result:"<<mRecurFunc->fibonacci(InputData)<<endl;
    }break;
    case TypeAckerman:{
        cout<<"Ackerman Test->Input Num:";
        uint32_t n,m;
        cin>>n;
        cin>>m;
        cout<<"Input ok->"<<"n="<<n<<";m="<<m<<endl;
        cout<<"Ackerman Test->Result:"<<mRecurFunc->Ackerman(n,m)<<endl;
    }break;
    case TypePerm:{
        cout<<"Perm Test->Input list size:";
        uint16_t size;
        cin>>size;
        int32_t listdata[10];
        for (int16_t i = 0; i < size; i++) {
            cin>>listdata[i];
        }
        cout<<"list ok.next,k and m:";
        uint32_t k,m;
        cin>>k;
        cin>>m;
        cout<<"Input ok->";
        for(int16_t i=0;i<size;i++){
            cout<<listdata[i]<<" ";
        }
        cout<<"+";
        cout<<"k="<<k<<";m="<<m<<endl;
        cout<<"Perm Test->Result:"<<endl;
        mRecurFunc->Perm(listdata,k,m);
    }break;
    case TypeqDivision:{
        cout<<"Internal Division->Input Num:";
        uint32_t n,m;
        cin>>n;
        cin>>m;
        cout<<"Input ok->"<<"n="<<n<<";m="<<m<<endl;
        cout<<"Internal Division Test->Result:"<<mRecurFunc->qDivision(n,m)<<endl;
    }break;
    case TypeHanoi:{
        cout<<"Hanoi->Input Num:";
        uint32_t n;
        cin>>n;
        cout<<"Input ok->"<<"n="<<n<<";"<<endl;
        cout<<"Hanoi Test->Result:"<<endl;
        mRecurFunc->hanoi(n,'a','b','c');
    }break;
    case TypeBinarySearch:{
        cout<<"BinarySearch->Input Num:";
        int16_t data[5]={-1,-2,321,2,4};
        for(int16_t i=0;i<5;i++){
            cout<<data[i]<<"; ";
        }
        cout<<endl;

        cout<<"BinarySearch Test->Result:"<<mRecurFunc->binarySearch(data,321,5)<<endl;
    }break;
    default:
        break;
    }
    cout<<"*********************************************"<<endl;
    return a.exec();
}
