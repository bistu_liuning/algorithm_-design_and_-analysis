﻿/**
 * @file recursionfunc.cpp
 * @brief   递归调用算法库
 * @author LiuNing (ning.liu@bistu.edu.cn)
 * @version 1.0
 * @date 2022-09-03
 * 
 * @copyright Copyright (c) 2022  高动态导航技术北京市重点实验室
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>日期       <th>版本     <th>作者     <th>描述     <th>时间
 * <tr><td>2022-09-03 <td>1.0     <td>刘宁     <td>上课用程序     <td>2108111207
 * </table>
 */
#include "recursionfunc.h"

recursionFunc::recursionFunc()
{

}

/**
 * @brief 阶乘
 * @param[in] n 输入参数
 * @return uint32_t 返回计算结果
 */
uint32_t recursionFunc::factorial(uint32_t n){
   if(n==0) return 1;
   return n*factorial(n-1);
}
/**
 * @brief fabonacci数列
 * @param[in] n 输入参数
 * @return uint32_t 返回计算结果
 */
uint32_t recursionFunc::fibonacci(uint32_t n){
    if(n<=1) return 1;
    return fibonacci(n-1)+fibonacci(n-1);
}
/**
 * @brief 递归Ackerman，p17
 * @param[in] n 输入参数
 * @param[in] m 输入参数
 * @return uint32_t 返回计算结果
 */
uint32_t recursionFunc::Ackerman(uint32_t n, uint32_t m){
    if(n == 0)
        return m+1;
    else if(m == 0)
        return Ackerman(n-1,1);
    else
        return Ackerman(n-1,Ackerman(n,m-1));
}
/**
 * @brief 非递归方式ackerman
 * @param[in] M 输入参数
 * @param[in] N 输入参数
 * @return int32_t 返回结果
 */
int32_t recursionFunc::AckermanNonRec(int32_t M,int32_t N)
{
    int top=0,m,n;
    int stack[10000][4]={{M,N}};//记录信息m,n,Ack(m,n),跳转出口种类;
    while (true)
    {
        m=stack[top][0];
        n=stack[top][1];
        if (m==0)
            stack[top][2]=n+1;
        else if (n==0)
        {
            top++;
            stack[top][0]=m-1;
            stack[top][1]=1;
            stack[top][3]=1;
            continue;
            l1:
            stack[top][2]=stack[top+1][2];
        }
        else
        {
            top++;
            stack[top][0]=m-1;
            top++;
            stack[top][0]=m;
            stack[top][1]=n-1;
            stack[top][3]=2;
            continue;
            l2:
            stack[top][1]=stack[top+1][2];
            stack[top][3]=3;
            continue;
            l3:
            stack[top][2]=stack[top+1][2];
        }
        if (top==0)
            break;
        top--;
        switch (stack[top+1][3])
        {
            case 1:goto l1;
            case 2:goto l2;
            case 3:goto l3;
        }
    }
    return stack[0][2];
}
/**
 * @brief 排列问题，p18
 * @param[inout] list 输入待排序数组
 * @param[in] k  数组长度
 * @param[in] m 排序长度
 */
void recursionFunc::Perm(int32_t* list,int32_t k,int32_t m)
{
     if(k == m)
     {
       for(int32_t i=0;i<k;++i){
           cout<<list[i];
       }
       cout<<endl;
     }
     else
     {
       for(int32_t i=m;i<k;++i)       {
           std::swap(list[i],list[m]);
           Perm(list,k,m+1);
           std::swap(list[i],list[m]);
       }
     }
}
/**
 * @brief 整数划分函数，p19
 * @param[in] n 输入参数待划分数
 * @param[in] m 输入最大化分数
 * @return 返回值
 */
uint32_t recursionFunc::qDivision(uint32_t n,uint32_t m)
{
    if(n<1||m<1)return 0;
    if(n==1||m==1)return 1;
    if(n<m)return qDivision(n,n);
    if(n==m)return qDivision(n,m-1)+1;
    return qDivision(n,m-1)+qDivision(n-m,m);
}
/**
 * @brief recursionFunc::move
 * @param A
 * @param n
 * @param C
 */
void recursionFunc::move(int8_t A,uint32_t n,int8_t C)       //搬动操作
{
    static int16_t m=0;
    cout<<"Step"<<++m<<" :"<<"  plane number is  "<<n<<"; move:  "<<A<<"->"<<C<<";"<<endl;
}
/**
 * @brief 汉诺塔问题
 * @param[in] n 圆盘数量
 * @param[in] a 柱a
 * @param[in] b 柱b
 * @param[in] c 柱c
 * @note abc输入为字符
 */
void recursionFunc::hanoi(uint32_t n,int8_t a,int8_t b,int8_t c)   //将塔座A上的n个圆盘按规则搬到C上
{
    if(n==1)
      move(a,1,c);
    else
    {
        hanoi(n-1,a,c,b);     //将A上编号为1至n-1的圆盘移到B，C做辅助塔
        move(a,n,c);          //将编号为n的圆盘从A移到C
        hanoi(n-1,b,a,c);     //将B上编号为1至n-1的圆盘移到C，A做辅助塔
    }
}
/**
 * @brief 二分搜索
 * @param[in] a
 * @param[in] x
 * @param[in] n
 * @return int16_t
 */
int16_t recursionFunc::binarySearch(int16_t* a,int16_t x,uint16_t n){
    int16_t left=0;
    int16_t right=n-1;
    while (left<=right) {
        int16_t middle=(left+right)/2;
        if(x==a[middle]) return middle;
        if(x>a[middle])left=middle+1;
        else right=middle-1;
    }
    return -1;
}
/**
 * @brief 二分法计算指数幂
 * @param[in] a 计算底数
 * @param[in] n 计算指数
 * @return 结果
 */
double recursionFunc::binaryPow(double a,int16_t n){
    if(n==0){
        return 1;
    }
    double b = binaryPow(a,n/2);
    b=b*b;
    if(n%2==1){
        b=b*a;
    }
    return b;
}

