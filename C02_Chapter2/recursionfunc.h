/**
 * @file recursionfunc.h
 * @brief 递归调用头文件
 * @author LiuNing (ning.liu@bistu.edu.cn)
 * @version 1.0
 * @date 2022-09-05
 * 
 * @copyright Copyright (c) 2022  高动态导航技术北京市重点实验室
 * 
 * @par 修改日志:
 * <table>
 * <tr><th>日期       <th>版本     <th>作者     <th>描述     <th>时间
 * <tr><td>2022-09-03 <td>1.0     <td>刘宁     <td>上课递归与分治用头文件     <td>2108111207
 * </table>
 */
#ifndef RECURSIONFUNC_H
#define RECURSIONFUNC_H

#include <gloabaluserdefine.h>

/** @enum RecursionType
 * @brief 运行不同的递归示例
 */
enum RecursionType{
    TypeFactorial=0,         ///< 阶乘
    TypeFibonacci,           ///< fibonacci数列
    TypeAckerman,
    TypePerm,
    TypeAckermanNonRec,
    TypeqDivision,
    TypeHanoi,
    TypeBinarySearch
};

/** @class recursionFunc
 * @brief The recursionFunc class
 */
class recursionFunc
{
public:
    recursionFunc();

    uint32_t factorial(uint32_t n);
    uint32_t fibonacci(uint32_t n);
    uint32_t Ackerman(uint32_t n, uint32_t m);
    int32_t AckermanNonRec(int32_t M,int32_t N);
    void Perm(int32_t* arr,int32_t size,int32_t N);
    uint32_t qDivision(uint32_t n,uint32_t m);
    void hanoi(uint32_t n,int8_t a,int8_t b,int8_t c);
    int16_t binarySearch(int16_t* a,int16_t x,uint16_t n);
    double binaryPow(double a,int16_t n);
private:
    void move(int8_t A,uint32_t n,int8_t C);
};

#endif // RECURSIONFUNC_H
